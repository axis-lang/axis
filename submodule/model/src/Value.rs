/**
 * Un valor representa un fujo de informacion (una arista) en los grafos de
 * representacion interna del programa.
 *
 * un valor posee diferentes tipos de atrubutos, los atributos
 * rasgos (Trairs) que caracterizan el comportamiento del valor.
 *
 *
 * **Tipologia de atributos de un valor:**
 *  - Lifetime: referencias a los nodos de construccion y destruccion del valor.
 *  - Accessor: informacion sobre la forma de acceder al valor
 *
 */

trait Lifetime {
}

trait Accessor {
}

struct Val<T: ?Unsized> {

}

