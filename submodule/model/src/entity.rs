use std::sync::Arc;


/**
 * An entity is a global (but may be anonymous) component.
 * 
 */
pub trait Entity {

    //fn layout() -> Optional<Layout>;
}

pub trait Layout {
    fn bitlenght(&self) -> usize;

}

pub mod layout {
    pub enum Type {
        Basic,
        Tuple, 
        Array
    }

    pub struct Basic {
        pub bitlenght: usize,
    }
    pub struct Tuple {

    }
    pub struct Array {

    }

    impl super::Layout for Basic {
        fn bitlenght(&self) -> usize {
            self.bitlenght
        }
    }
}

pub fn layout() -> Arc<dyn Layout> {
    // usara el internalizador para retornar un Inm
    return Arc::new(layout::Basic{bitlenght:32});
}


pub trait Descriptor: Entity {
    fn layout(&self) -> Arc<dyn Layout>;
}

pub trait Reference {

}

pub type Pointer<T> = *mut T;

impl<T> Reference for Pointer<T> {

}



pub struct Value<R: Reference> {
    pub descriptor: Arc<dyn Descriptor>, // PV<dyn Descriptor>
    pub reference: R
}

fn test() {
    let _a: Value<Pointer<u32>>;
}