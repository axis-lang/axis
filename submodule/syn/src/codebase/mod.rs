use std::{path::PathBuf, fs};

use dashmap::DashMap;
use eyre::{Result, eyre};

use crate::ast;

#[salsa::input]
pub struct SourceUnit {
    //codebase: Codebase,
    pub filepath: PathBuf,
    #[return_ref]
    pub contents: String,
    //#[return_ref]
    //deltas: String, incremental deltas..
}

#[salsa::tracked]
impl SourceUnit {
    #[salsa::tracked]
    pub fn ast(self, db: &dyn Db) -> ast::SourceUnit {
        let parser = ast::Parser::new(db);
        parser.parse(self.contents(db))
    }
}

#[salsa::jar(db = Db)]
pub struct Jar(SourceUnit, SourceUnit_ast, ast::Symbol);

pub trait Db: salsa::DbWithJar<Jar> {}

#[salsa::db(Jar)]
pub struct Codebase {
    pub storage: salsa::Storage<Self>,
    pub root_dir: PathBuf,
    pub source_units: DashMap<PathBuf, SourceUnit>,
    //pub entities: DashMap<Path, Entity>,
}

// Database is codebase?..
impl Codebase {
    pub fn new(root_dir: PathBuf) -> Result<Codebase> {
        let root_dir = root_dir.canonicalize()?;
        Ok(Codebase {
            storage: Default::default(),
            root_dir,
            source_units: Default::default(),
        })
    }

    pub fn source_unit(&self, filename: PathBuf) -> Result<SourceUnit> {
        let mut filepath = self.root_dir.clone();
        filepath.push(filename);
        let path = filepath.canonicalize()?;
        
        if !path.starts_with(&self.root_dir) {
            return Err(eyre!("new error message"));
        }
        //let filepath = self.root_dir.push(filepath).canonicalize()?;
        let contents = fs::read_to_string(&path)?;
        Ok(SourceUnit::new(self, path, contents))
    }
}

// #[salsa::tracked]
// pub fn process_manifest(wk: &Database) {
//     // let manifest_filepath = workspace_dir.with_file_name("axis.toml");
//     // let manifest = fs::read_to_string(manifest_filepath)?;
//     // let manifest: manifest::Workspace = toml::from_str(&manifest)?;
// }

impl crate::Db for Codebase {}

impl salsa::Database for Codebase {}

