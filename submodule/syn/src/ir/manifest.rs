
use std::path::PathBuf;



#[derive(Clone, Debug, PartialEq)]
pub struct Workspace {
    pub codebases: Vec<Codebase>
    //pub stack: 
}


#[derive(Clone, Debug, PartialEq)]
pub struct Codebase {
    pub scope: String, // identificador unico (junto con version)
    pub version: String, //
    pub namespace: String, // cadena en formato uri por ejemplo: std/core para std::core
    pub source_root: PathBuf,
    // .. stack..
}