/*
Union, 
union::Discriminator
union::dicriminator::Strategy
union::dicriminator::strategy::Auto
union::dicriminator::strategy::OuterBitmap
union::dicriminator::strategy::OuterModularStack
union::dicriminator::strategy::InnerZeroValue
union::dicriminator::strategy::InnerFloatNans
union::dicriminator::strategy::InnerRange
*/

pub trait Value<'z> {

    fn is_const(&self) -> bool;
    fn is_var(&self) -> bool;

    fn as_const(&self) -> Option<&dyn value::Const<'z>>;
    fn as_var(&self) -> Option<&dyn value::Var<'z>>;
    
    
    //fn ty(&self) -> Option<&dyn Value<'z>>;
    //fn domain(&self) -> Option<>;
    // el dominio tiene tipos, 
}

mod value {
    use super::Value;
    pub trait Const<'z>: Value<'z> {

        // el tipo de una constante siemre sera constante
        fn ty(&self) -> Option<&dyn Const<'z>>;

    }
    pub trait Var<'z>: Value<'z> {
        // el tipo de una variable puede ser 
    }
}

trait Domain<'z> {

}