# Axis Model

La representacion interna en axis es realizada mediante un grafo hibrido entre CFG ()Control Flow Graph) y V(S)DG (Value (state) Dependency Graph).

Para representar estos grafos visualmente en axis se recurre a un sistema de capas, el CFG ocurre en el fondo las aristas representan los saltos y los nodos se ejecutan de izquierda a derecha y de arriba a abajo, las iteraciones se representan como secuencias horizontales de nodos con una arista que retorna

**VSDG over CFG**

## Value


## Operation

## Implementation tools

Self referentiable structs

https://github.com/joshua-maros/ouroboros/tree/79bac55f29edbb44f7205246ab5c3f706cbc4647/ouroboros

## References

[SPA](https://cs.au.dk/~amoeller/spa/spa.pdf "Static Program Analiysis")

https://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-705.pdf
