use std::{path::PathBuf, str::FromStr};

use axis_engine::*;



#[test]
fn unit_outline() {
    let db = Codebase::new(PathBuf::from_str("testsuite").unwrap()).unwrap();

    let unit = db.source_unit(PathBuf::from_str("basic.ax").unwrap()).unwrap();

    let ast = dbg!(unit.ast(&db));

    for a in ast {
        dbg!(a);
    }
    //let a: _ = ast.iter().map(|item| dbg!(item)).collect();

}
