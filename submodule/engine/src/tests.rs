#![cfg(test)]

use std::path::PathBuf;

use super::*;
pub use ariadne::{Label, Report, ReportKind, Source};
use include_dir::{include_dir, Dir};

pub static TESTSUITE: Dir<'_> = include_dir!("$CARGO_MANIFEST_DIR/testsuite");

pub fn testsuite_file_contents(filename: &str) -> &str {
    TESTSUITE
        .get_file(filename)
        .unwrap()
        .contents_utf8()
        .unwrap()
}

#[test]
fn reading_and_writing_code() {
    use crate::ast::rendering::Writer;

    let db = Codebase::new(PathBuf::from("testsuite")).unwrap();
    let source_unit = db.source_unit(PathBuf::from("basic.ax")).unwrap();

    dbg!(source_unit.contents(&db));

    //let db: Codebase = Default::default();
    let parser = ast::Parser::new(&db);

    let writer_ctx = ast::rendering::tests::test_ctx(&db, 80);

    let contents = testsuite_file_contents("basic.ax");
    let ast = parser.parse(contents);
    dbg!(ast);

    //parsing::source_unit(&parse_ctx).parse_recovery(contents);
    /*
            let (ast, errors) = parsing::source_unit(&parse_ctx).parse_recovery(contents);

            if let Some(ast) = ast {
                for item in ast {
                    dbg!(item.render(&writer_ctx));
                }
            }
    */
}
