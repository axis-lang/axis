pub type Ref<T> = Box<T>;
pub type Span = std::ops::Range<usize>;
pub type WithError<T> = Result<T, Error>;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Error {
    Span(Span),
}

#[salsa::interned]
pub struct Symbol {
    #[return_ref]
    pub text: String,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Ident {
    pub symbol: Symbol,
    pub span: Span,
}

pub mod ident {
    use super::*;

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub enum Optional {
        Ident(Ident),
        Annon(Span),
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Path {
    pub parts: Vec<path::Part>,
}

pub mod path {
    use super::*;

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub struct Part {
        pub ident: Ident,
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Field {
    Ident(Ident),
    Nat(value::Nat),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Value {
    Nat(value::Nat), // Natural (with 0 ;)
    Rat(value::Rat), // Rational
    Tex(value::Tex),
}

pub mod value {
    use super::*;

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub struct Nat {
        pub value: usize,
        pub span: Span,
    }

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub struct Rat {
        //pub value: f64,
        pub value: u64,
        pub span: Span,
    }

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub struct Tex {
        pub value: String,
        pub span: Span,
    }

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub enum Expr {
        Error(Span),
        //
        Literal(Value),
        // TextInterpolation
        Path(Path),
        //Tuple(Vec<tuple::Item>), // ( .. )
        //List(Vec<Expr>),  // [ .. ]
        Block(Block),                         // { .. }
        Get(Ref<Expr>, Field),                // expr . field
        Call(Ref<Expr>, Vec<call::Argument>), // a(b...)
        //Index(Ref<Expr>, Vec<expr::Indice>), // a[b...]
        UnaryOp(UnaryOp, Ref<Expr>),
        BinaryOp(BinaryOp, Ref<Expr>, Ref<Expr>),

        If(Ref<Expr>, Block, Option<Block>),
    }

    pub mod tuple {
        use super::*;

        #[derive(Clone, Debug, PartialEq, Eq)]
        pub enum Item {
            Value(Option<Field>, Expr), // field: expr
            Source(Expr),               // ..expr
        }
    }

    pub type Block = Result<Vec<block::Item>, Error>;

    pub mod block {
        use super::*;

        #[derive(Clone, Debug, PartialEq, Eq)]
        pub enum Item {
            // AKA Statement
            Noop(Span), // _
            Expr(Expr),
            Let(pattern::Expr, Option<r#type::Expr>, Option<value::Expr>),
            //Err(Span),
        }

        pub type Items = Vec<Result<Item, Error>>;
    }

    pub mod call {
        use super::*;

        #[derive(Clone, Debug, PartialEq, Eq)]
        pub enum Argument {
            Expr(Option<Field>, Expr), // field: expr
            Source(Expr),              // ..expr
        }
    }

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub enum UnaryOp {
        Neg(Span),
        Not(Span),
    }

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub enum BinaryOp {
        Add(Span),
        Sub(Span),
        Mul(Span),
        Div(Span),
        Mod(Span),
        And(Span),
        Or(Span),
        BitAnd(Span),
        BitOr(Span),
        BitXor(Span),
        BitShiftLeft(Span),
        BitShiftRight(Span),
        Eq(Span),
        NotEq(Span),
        Gt(Span),
        GtOrEq(Span),
        Lt(Span),
        LtOrEq(Span),
    }

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub enum Indice {}
}

pub mod r#type {
    use super::*;
    #[derive(Clone, Debug, PartialEq, Eq)]
    pub enum Expr {
        Error(Span),
        Path(Path),
    }
}

pub mod pattern {
    use super::*;

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub enum Expr {
        Error(Span),
        Path(Path),
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Function {
    pub ident: Ident,
    pub params: Option<Vec<function::Parameter>>,
    pub r#return: Option<r#type::Expr>,
    pub body: Option<value::Block>,
}

pub mod function {
    use super::*;

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub struct Parameter {
        pub mutability: Option<parameter::Mutability>,
        pub ident: ident::Optional,
        pub r#type: Option<r#type::Expr>,
        pub value: Option<value::Expr>,
    }

    pub mod parameter {
        use super::*;

        #[derive(Clone, Debug, PartialEq, Eq)]
        pub enum Mutability {
            Inmutable(Span),
            Mutable(Span),
            Owned(Span),
        }
    }
}

pub mod module {
    use super::*;
    #[derive(Clone, Debug, PartialEq, Eq)]
    pub enum Item {
        Function(Function),
    }
}


pub type SourceUnit = Vec<module::Item>;