//https://docs.rs/pretty/latest/pretty/index.html

use std::rc::Rc;

use pretty::{DocAllocator, RcDoc};

use crate::ast::*;

macro_rules! seq {
    ( $p0:expr, $($pn:expr),* $(,)?) => {
        $p0 $(.append($pn) )*
    }
}

#[derive(Clone)]
pub struct Ctx<'db> {
    pub db: &'db dyn crate::Db,
    //pub allocator: DocAllocator<'db, >,
    pub width: usize,
    pub duoble_quotes: RcDoc<'db, ()>,
}

pub trait Writer {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()>;

    fn render(&self, ctx: &Ctx) -> String {
        let mut w = Vec::new();
        self.to_doc(ctx).render(ctx.width, &mut w).unwrap();
        String::from_utf8(w).unwrap()
    }
}

impl Writer for Symbol {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        RcDoc::as_string(self.text(ctx.db))
    }
}

impl Writer for Ident {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        self.symbol.to_doc(ctx)
    }
}

impl Writer for ident::Optional {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        match self {
            ident::Optional::Ident(ident) => ident.to_doc(ctx),
            ident::Optional::Annon(_) => RcDoc::as_string("_"),
        }
    }
}

impl Writer for Path {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        RcDoc::intersperse(
            self.parts.iter().map(|p| p.to_doc(ctx)),
            RcDoc::as_string("::"),
        )
    }
}

impl Writer for path::Part {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        self.ident.to_doc(ctx)
    }
}

impl Writer for Value {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        match self {
            Value::Nat(nat) => nat.to_doc(ctx),
            Value::Rat(nat) => nat.to_doc(ctx),
            Value::Tex(nat) => nat.to_doc(ctx),
        }
    }
}
impl Writer for value::Nat {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        RcDoc::as_string(self.value)
    }
}

impl Writer for value::Rat {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        RcDoc::as_string(self.value)
    }
}

impl Writer for value::Tex {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        RcDoc::text("\"")
            .append(RcDoc::text(self.value.clone()))
            .append(RcDoc::text("\""))
    }
}

impl Writer for value::Expr {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        use value::Expr;

        match self {
            Expr::Literal(expr) => expr.to_doc(ctx),
            Expr::Path(expr) => expr.to_doc(ctx),
            Expr::BinaryOp(op, l, r) => seq!(l.to_doc(ctx), op.to_doc(ctx), r.to_doc(ctx)),
            _ => RcDoc::text("(**ast-error**)"),
        }
    }
}

impl Writer for value::BinaryOp {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        use value::BinaryOp;
        match self {
            BinaryOp::Add(_) => RcDoc::text(" + "),
            BinaryOp::Sub(_) => RcDoc::text(" - "),
            _ => RcDoc::text(" ? "),
        }
    }
}

impl Writer for r#type::Expr {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        use r#type::Expr;

        match self {
            Expr::Path(expr) => expr.to_doc(ctx),
            _ => RcDoc::text("(**ast-error**)"),
        }
    }
}
impl Writer for pattern::Expr {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        use pattern::Expr;

        match self {
            Expr::Path(expr) => expr.to_doc(ctx),
            _ => RcDoc::text("(**ast-error**)"),
        }
    }
}

impl Writer for value::block::Item {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        use value::block::Item;

        match self {
            Item::Noop(_) => RcDoc::text("_"),
            Item::Expr(e) => seq!(e.to_doc(ctx), RcDoc::text(";"), RcDoc::line()),
            Item::Let(a, b, c) => {
                let mut doc = RcDoc::text("let").append(a.to_doc(ctx));
                if let Some(b) = b {
                    doc = doc.append(RcDoc::text(":")).append(b.to_doc(ctx))
                }
                if let Some(c) = c {
                    doc = doc.append(RcDoc::text("=")).append(c.to_doc(ctx))
                }
                doc.append(RcDoc::line())
            }
            _ => RcDoc::text("not implemented;"),
        }
    }
}

impl Writer for Function {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        let mut ret = seq!(RcDoc::text("fn"), RcDoc::space(), self.ident.to_doc(ctx),);

        if let Some(params) = &self.params {
            ret = seq!(
                ret,
                RcDoc::text("("),
                RcDoc::intersperse(
                    params.iter().map(|param| param.to_doc(ctx)),
                    RcDoc::text(", ")
                ),
                RcDoc::text(")"),
            );
        }

        if let Some(r#return) = &self.r#return {
            ret = seq!(
                ret,
                RcDoc::text(" -> "),
                r#return.to_doc(&ctx),
                RcDoc::space()
            );
        }

        if let Some(body) = &self.body {
            ret = ret.append(body.to_doc(ctx));
        }

        ret
    }
}

impl Writer for value::Block {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        //use value::block::Item;
        match self {
            Ok(items) => seq!(
                RcDoc::text("{"),
                RcDoc::hardline().nest(4).append(RcDoc::intersperse(
                    items.iter().map(|i| i.to_doc(ctx)),
                    RcDoc::hardline()
                )),
                RcDoc::text("}")
            ),
            Err(_) => RcDoc::text("{**ast-error**}"),
        }
        .group()
    }
}

impl Writer for function::Parameter {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        seq!(
            self.mutability
                .as_ref()
                .map_or_else(|| RcDoc::nil(), |m| m.to_doc(&ctx)),
            RcDoc::space(),
            self.ident.to_doc(&ctx),
            self.r#type.as_ref().map_or_else(
                || RcDoc::nil(),
                |t| RcDoc::text(": ").append(t.to_doc(&ctx))
            ),
        )
    }
}

impl Writer for function::parameter::Mutability {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        match self {
            Self::Inmutable(_) => RcDoc::text("inm"),
            Self::Mutable(_) => RcDoc::text("mut"),
            Self::Owned(_) => RcDoc::text("own"),
        }
        .append(RcDoc::space())
    }
}

impl Writer for module::Item {
    fn to_doc(&self, ctx: &Ctx) -> RcDoc<()> {
        use module::Item;
        match self {
            Item::Function(f) => f.to_doc(ctx),
            _ => RcDoc::text("(**ast-error**)"),
        }
    }
}

#[cfg(testum)]
pub mod tests {
    use super::*;
    use crate::*;
    //use crate::test::*;

    pub fn test_ctx<'db>(db: &'db dyn crate::Db, width: usize) -> Ctx<'db> {
        Ctx {
            db,
            width,
            duoble_quotes: RcDoc::text("\""),
        }
    }

    #[test]
    fn test() {
        let db: Codebase = Default::default();
        let ctx = test_ctx(&db, 80);

        let sym = Symbol::new(ctx.db, "hey joe".to_string());

        assert_eq!(sym.render(&ctx), "hey joe".to_string());
    }
}
