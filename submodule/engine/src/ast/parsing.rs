extern crate chumsky;
use std::{fmt};

use super::nodes::*;

use chumsky::prelude::{Parser, *};

pub type SyntaxError = chumsky::error::Simple<char>; // diagnostic???

//pub type DynParser<T> = Box<dyn chumsky::Parser<char, T, Error = SyntaxError>>;

#[derive(Clone)]
pub struct AxisParser<'a>(BoxedParser<'a, char, Vec<module::Item>, SyntaxError>);


impl<'db> fmt::Debug for AxisParser<'db> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Parser")
    }
}

impl<'db> Eq for AxisParser<'db> {}

impl<'db> PartialEq for AxisParser<'db> {
    fn eq(&self, other: &Self) -> bool {
        true
    }
}


impl<'ctx> AxisParser<'ctx> {
    pub fn new(db: &'ctx dyn crate::Db) -> AxisParser {
        let ctx = Ctx::new(db);
        AxisParser(source_unit(&ctx).boxed())
    }

    pub fn parse(&self, stream: &str) -> SourceUnit {
        match self.0.parse_recovery(stream) {
            (Some(ast), _) => ast,
            (None, errors) => vec![], // TODO: Translate errors in module::items
        }
    }
}

pub type RecursiveParser<'a, T> = Recursive<'a, char, T, SyntaxError>;

macro_rules! any {
        ( $p0:expr, $($pn:expr),* $(,)?) => {
            $p0 $(.or($pn) )*
        }
    }

macro_rules! choice {
        ( $($options:expr),* $(,)?) => {
            choice(( $($options),* ))
        }
    }

macro_rules! seq {
        ( $p0:expr, $($pn:expr),* $(,)?) => {
            $p0 $(.then($pn) )*
        }
    }

macro_rules! to {
        ({ $($items:ident),* $(,)* } as $to:path) => {
            | to!{@ [] [ $( $items )* ]} | { $to { $( $items ),* } }
        };
        (( $($items:ident),* $(,)* ) as $to:path) => {
            | to!{@ [] [ $( $items )* ]} | { $to ( $( $items ),* ) }
        };

        (@ [] [$item1:ident $item2:ident $($items:ident)*] ) => {
            to!{@ [( $item1 , $item2 )] [$($items)*]}
        };
        (@ [$($res:tt)+] [$item:ident $($items:ident)*] ) => {
            to!{@ [( $($res)* , $item )] [$($items)*]}
        };
        (@ [] [$item:ident] ) => {
            $item
        };
        (@ [$($res:tt)*] [] ) => {
            $($res)*
        };
    }

macro_rules! apply_unary_ops {
    ($parser:expr, $ops:expr, $to:path) => {
        $ops.repeated()
            .then($parser)
            .foldr(|op, v| $to(op, Box::new(v)))
    };
}

macro_rules! apply_binary_ops {
    ($parser:expr, $ops:expr, $to:path) => {
        $parser
            .clone()
            .then($ops.then($parser).repeated())
            .foldl(|l, (op, r)| $to(op, Box::new(l), Box::new(r)))
    };
}

macro_rules! op {
    ($just:literal as $map:expr) => {
        just($just).map_with_span(|_, span| $map(span)).padded()
    };
}

macro_rules! qual {
    ($just:literal as $map:path) => {
        just($just).map_with_span(|_, span| $map(span)).padded()
    };
}

macro_rules! kw {
    ($lit:literal) => {
        just($lit).padded()
    };
}

macro_rules! pm {
    ($lit:literal) => {
        just($lit).padded()
    };
}

macro_rules! enclosed_by {
    ($ldel:expr, $expr:expr, $sep:expr, $rdel:expr) => {
        $expr
            .separated_by(pm!($sep))
            .allow_trailing()
            .delimited_by(pm!($ldel), pm!($rdel))
    };
}

#[derive(Clone)]
pub struct Ctx<'db> {
    pub db: &'db dyn crate::Db,
    pub type_expr: RecursiveParser<'db, r#type::Expr>,
    pub value_expr: RecursiveParser<'db, value::Expr>,
    pub value_block: RecursiveParser<'db, value::Block>,
    pub pattern_expr: RecursiveParser<'db, pattern::Expr>,
    //pub module_items: RecursiveParser<'db, Vec<module::Item>>,
}

pub fn ident<'ctx>(
    ctx: &Ctx<'ctx>,
) -> impl Parser<char, Ident, Error = SyntaxError> + Clone + 'ctx {
    text::ident()
        .map_with_span(|text, span| Ident {
            symbol: Symbol::new(ctx.db, text),
            span,
        })
        .padded()
}

pub fn path<'ctx>(ctx: &Ctx<'ctx>) -> impl Parser<char, Path, Error = SyntaxError> + Clone + 'ctx {
    ident(ctx)
        .map(to!({ ident } as path::Part))
        .separated_by(just("::").padded())
        .at_least(1)
        .map(to!({ parts } as Path))
}

pub fn value_nat() -> impl Parser<char, value::Nat, Error = SyntaxError> + Clone {
    text::int(10)
        .from_str::<usize>()
        .unwrapped()
        .map_with_span(|value, span| value::Nat { value, span })
}

pub fn value() -> impl Parser<char, Value, Error = SyntaxError> + Clone {
    //choice!(
    value_nat().map(Value::Nat)
    //)
}

pub fn field<'ctx>(
    ctx: &Ctx<'ctx>,
) -> impl Parser<char, Field, Error = SyntaxError> + Clone + 'ctx {
    choice!(value_nat().map(Field::Nat), ident(ctx).map(Field::Ident))
}

fn value_expr<'ctx>(
    ctx: &Ctx<'ctx>,
) -> impl Parser<char, value::Expr, Error = SyntaxError> + Clone + 'ctx {
    let block = ctx.value_block.clone().map(value::Expr::Block);
    let path = path(ctx).map(value::Expr::Path).padded();
    let literal = value().map(value::Expr::Literal).padded();

    let if_block = kw!("if")
        .ignore_then(
            ctx.value_expr
                .clone()
                .recover_with(skip_until(['{'], |span| value::Expr::Error(span)))
                .map(Box::new),
        )
        .then(ctx.value_block.clone())
        .then(kw!("else").ignore_then(ctx.value_block.clone()).or_not())
        .map(to!((a, b, c) as value::Expr::If));

    let terminal = if_block.or(choice!(path, literal, block));

    let get = terminal
        .then(pm!('.').ignore_then(field(ctx)).repeated())
        .foldl(|l, r| value::Expr::Get(Box::new(l), r));

    let call_argumnet = choice!(
        field(ctx)
            .then_ignore(pm!(':'))
            .or_not()
            .then(ctx.value_expr.clone())
            .map(to!((field, value) as value::call::Argument::Expr)),
        pm!("..")
            .ignore_then(ctx.value_expr.clone())
            .map(value::call::Argument::Source)
    );

    let call = get
        .then(enclosed_by!('(', call_argumnet, ',', ')').repeated())
        .foldl(|l, r| value::Expr::Call(Box::new(l), r));

    // Arithmetic
    let expr = apply_unary_ops!(
        call,
        choice!(
            op!('-' as value::UnaryOp::Neg),
            op!('!' as value::UnaryOp::Not),
        ),
        value::Expr::UnaryOp
    );

    let expr = apply_binary_ops!(
        expr,
        choice!(
            op!('*' as value::BinaryOp::Mul),
            op!('/' as value::BinaryOp::Div),
            op!('%' as value::BinaryOp::Mod),
        ),
        value::Expr::BinaryOp
    );

    let expr = apply_binary_ops!(
        expr,
        choice!(
            op!('+' as value::BinaryOp::Add),
            op!('-' as value::BinaryOp::Sub)
        ),
        value::Expr::BinaryOp
    );

    //  Comparison
    let expr = apply_binary_ops!(
        expr,
        choice!(
            op!("==" as value::BinaryOp::Eq),
            op!("!=" as value::BinaryOp::NotEq),
            op!(">" as value::BinaryOp::Gt),
            op!(">=" as value::BinaryOp::GtOrEq),
            op!("<" as value::BinaryOp::Lt),
            op!("=<" as value::BinaryOp::LtOrEq),
        ),
        value::Expr::BinaryOp
    );

    expr //.recover_with(skip_until([';', '}'], |span| value::Expr::Error(dbg!(span))))
}

fn value_block<'ctx>(
    ctx: &Ctx<'ctx>,
) -> impl Parser<char, value::Block, Error = SyntaxError> + Clone + 'ctx {
    choice!(
        op!('_' as value::block::Item::Noop),
        kw!("let")
            .ignore_then(
                ctx.pattern_expr
                    .clone()
                    .recover_with(skip_until([':'], |span| pattern::Expr::Error(span)))
            )
            .then(
                pm!(':')
                    .ignore_then(
                        ctx.type_expr
                            .clone()
                            .recover_with(skip_until(['='], |span| r#type::Expr::Error(span)))
                    )
                    .or_not()
            )
            .then(
                pm!('=')
                    .ignore_then(
                        ctx.value_expr
                            .clone()
                            .recover_with(skip_until([';'], |span| value::Expr::Error(span)))
                    )
                    .or_not()
            )
            .map(to!((pat, typ, val) as value::block::Item::Let)),
        //if_block.map(value::block::Item::Expr),
        ctx.value_expr.clone().map(value::block::Item::Expr),
    )
    .then_ignore(pm!(';').or_not())
    .repeated()
    .delimited_by(pm!('{'), pm!('}'))
    .map(Ok)
}

pub fn type_expr<'ctx>(
    ctx: &Ctx<'ctx>,
) -> impl Parser<char, r#type::Expr, Error = SyntaxError> + Clone + 'ctx {
    path(ctx).map(r#type::Expr::Path)
}

pub fn pattern_expr<'ctx>(
    ctx: &Ctx<'ctx>,
    //    type_expr: impl Parser<char, r#type::Expr, Error = SyntaxError> + Clone + 'ctx,
) -> impl Parser<char, pattern::Expr, Error = SyntaxError> + Clone + 'ctx {
    path(ctx).map(pattern::Expr::Path)
}

pub fn function<'ctx>(
    ctx: &Ctx<'ctx>,
) -> impl Parser<char, Function, Error = SyntaxError> + Clone + 'ctx {
    use function::*;

    let parameter = seq!(
        choice!(
            qual!("mut" as parameter::Mutability::Mutable),
            qual!("inm" as parameter::Mutability::Inmutable),
            qual!("own" as parameter::Mutability::Owned),
        )
        .or_not(),
        choice!(
            ident(ctx).map(ident::Optional::Ident),
            qual!('_' as ident::Optional::Annon),
        ),
        pm!(':').ignore_then(type_expr(ctx)).or_not(),
        pm!('=').ignore_then(value_expr(ctx)).or_not(),
    )
    .map(to!({
            mutability,
            ident,
            r#type,
            value,
        } as Parameter ));

    kw!("fn")
        .ignore_then(ident(ctx).padded())
        .then(enclosed_by!('(', parameter, ',', ')').or_not())
        .then(pm!("->").ignore_then(type_expr(ctx)).or_not())
        .then(choice!(
            pm!(';').to(None),
            ctx.value_block.clone().map(Some)
        ))
        .map(to!({ ident, params, r#return, body} as Function))
}

pub fn module_items<'ctx>(
    ctx: &Ctx<'ctx>,
) -> impl Parser<char, Vec<module::Item>, Error = SyntaxError> + Clone + 'ctx {
    function(ctx).map(module::Item::Function).repeated()
}

pub fn source_unit<'ctx>(
    ctx: &Ctx<'ctx>,
) -> impl Parser<char, SourceUnit, Error = SyntaxError> + Clone + 'ctx {
    module_items(&ctx).then_ignore(end())
}

impl<'db> Ctx<'db> {
    pub fn new(db: &'db dyn crate::Db) -> Ctx<'db> {
        let mut ctx = Ctx {
            db,
            type_expr: Recursive::declare(),
            value_expr: Recursive::declare(),
            value_block: Recursive::declare(),
            pattern_expr: Recursive::declare(),
            //module_items: Recursive::declare(),
        };

        ctx.type_expr.define(type_expr(&ctx.clone()));
        ctx.value_expr.define(value_expr(&ctx.clone()));
        ctx.value_block.define(value_block(&ctx.clone()));
        ctx.pattern_expr.define(pattern_expr(&ctx.clone()));
        ctx
    }
}

#[cfg(testum)]
pub mod tests {
    use super::*;
    use crate::test::*;
    use crate::*;

    #[test]
    fn test() {
        let db: Codebase = Default::default();
        let ctx = Ctx::new(&db);

        let contents = testsuite_file_contents("basic.ax");

        // let contents = r##"
        //         fn foo() {
        //             let a: 5 = if v < 0 { -v } else { v };
        //             call(1,2,3, foo:34, ..alpha);
        //             _
        //         }
        //     "##;

        let parser = source_unit(&ctx);

        let (ast, errors) = parser.parse_recovery(contents);

        for err in dbg!(errors) {
            let report = Report::build(ReportKind::Error, (), 34);

            let report = match &err.reason() {
                chumsky::error::SimpleReason::Unexpected => report.with_label(
                    Label::new(err.span())
                        .with_message(format!("Unexpected char {:?}", err.found().unwrap())),
                ),
                chumsky::error::SimpleReason::Unclosed { span, delimiter } => report
                    .with_label(
                        Label::new(err.span())
                            .with_message(format!("Unclosed delimiter {:?}", err.found().unwrap())),
                    )
                    .with_label(Label::new(span.clone()).with_message(format!("Started here"))),
                _ => panic!(),
            };

            report.finish().eprint(Source::from(contents)).unwrap();
        }

        dbg!(ast);
    }
}
