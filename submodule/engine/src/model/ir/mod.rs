/*!
 * IR implementation based on RVSDG 
 * 
 * [RVSDG: An Intermediate Representation for Optimizing Compilers](https://arxiv.org/pdf/1912.05036.pdf)
 * 
 * El grafo es aciclico y se construye top/down respecto del flujo de datos
 * 
 * 
 * 
 */

pub enum NodeKind {
    // intra proc
    Gamma, // switch
    Delta, // Shared // or extern?
    Theta, // iter
    // interprocedural
    Lambda, // procedural block callee
    Apply, // caller kappa?
   
    Phi, // recursive
    Omega, // Translation unit
}



#[salsa::interned]
struct Region {

}

//!
//! Un procedimiento
//! 
//! tiene como inputs: imports, globals, parameters, states
pub struct Procedure {
    pub parameters: Vec<procedure::Parameter>,
    pub results: Vec<procedure::Result>
}
pub type Lambda = Procedure;

pub mod procedure {
    pub struct Parameter {
        // parameter es un value
    }

    pub struct Result {
        // result es un user
    }
}

pub type Gamma = Switch;
struct Switch {

}

pub type Theta = Iterative;
struct Iterative {

}

struct Apply {

}

pub type Phi = Recursive;
struct Recursive {

}

pub type Omega = Unit;
struct Unit {
    // imports / exports
}





