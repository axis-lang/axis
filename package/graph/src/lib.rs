/*!
 *
 */

use axis_pure::{Consign, Pure};




#[derive(Hash, PartialEq)]
struct NodeInput {
    origin: Pure<Apply>,
}


#[derive(Hash, PartialEq, Consign)]
struct Apply {
    function: NodeInput,
    argument: NodeInput,

}
