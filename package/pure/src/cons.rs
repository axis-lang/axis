use std::{
    hash::{Hash, Hasher},
    marker::PhantomData,
    ptr::NonNull,
    sync::atomic::{AtomicUsize, Ordering, fence},
};

use hashbrown::{Equivalent, HashMap};

#[derive(Default)]
pub struct ConsArena<T>(HashMap<ConsEntry<T>, ()>);

pub struct Cons<'a, T: 'a + ?Sized> {
    ptr: NonNull<ConsInner<T>>,
    phantom: PhantomData<&'a ConsInner<T>>,
}

struct ConsInner<T: ?Sized> {
    ref_count: AtomicUsize,
    value: T,
}

struct ConsKey<'a, T>(&'a T);

#[derive(PartialEq)]
struct ConsEntry<T>(NonNull<ConsInner<T>>);

impl<T: Hash + PartialEq> ConsArena<T> {
    pub fn consign<'a>(&'a mut self, value: T) -> Cons<'a, T> {
        let (entry, _) = self
            .0
            .raw_entry_mut()
            .from_key(&ConsKey(&value))
            .or_insert_with(|| (ConsEntry::new(value), ()));

        
        entry.inner().inc_ref()
    }
}

impl<T> ConsEntry<T> {
    fn new(value: T) -> Self {
        let inner = Box::new(ConsInner {
            ref_count: AtomicUsize::new(0),
            value,
        });

        ConsEntry(Box::leak(inner).into())
    }
}

impl<T: Hash + PartialEq> ConsInner<T> {
    fn inc_ref(&self) -> Cons<T> {
        if self.ref_count.fetch_add(1, Ordering::Relaxed) >= isize::MAX as usize {
            std::process::abort();
        }

        Cons {
            ptr: self.into(),
            phantom: PhantomData,
        }
    }

    fn dec_ref(&self) {
        if self.ref_count.fetch_sub(1, Ordering::Release) != 1 {
            return;
        }

        fence(Ordering::Acquire);

        //with_consign::<T, _, _>(|consign| consign.remove_entry(&ConsignKey(&self.data)));
    }
}

impl<T> ConsEntry<T> {
    #[inline]
    fn inner(&self) -> &ConsInner<T> {
        unsafe { self.0.as_ref() }
    }
}

impl<T> Cons<'_, T> {
    #[inline]
    fn inner(&self) -> &ConsInner<T> {
        unsafe { self.ptr.as_ref() }
    }
}

impl<T: Hash + PartialEq> Hash for ConsKey<'_, T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl<T: Hash + PartialEq> Hash for ConsEntry<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.inner().value.hash(state);
    }
}

//impl<T: Hash + PartialEq> Eq for ConsKey<'_, T> {}

impl<T: Hash + PartialEq> Equivalent<ConsEntry<T>> for ConsKey<'_, T> {
    fn equivalent(&self, entry: &ConsEntry<T>) -> bool {
        *self.0 == entry.inner().value
    }
}
