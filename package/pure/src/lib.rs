//#[macro_use]
extern crate axis_pure_derive;

use std::any::{Any, TypeId};
use std::fmt::{self, Debug, Display};
use std::hash::Hasher;
use std::marker::PhantomData;

use std::ops::Deref;
use std::ptr::NonNull;
use std::sync::atomic::Ordering;
use std::sync::Mutex;
use std::{hash::Hash, sync::atomic, sync::atomic::AtomicUsize};

pub use axis_pure_derive::Consign;
use hashbrown::{Equivalent, HashMap};
use lazy_static::lazy_static;

pub mod cons;

pub trait Consign
where
    Self: 'static + Sized + Send + Sync + Hash + PartialEq,
{
    #[inline]
    fn pure(self) -> Pure<Self> {
        Pure::new(self)
    }

    // consign_count
    fn consigned_count() -> usize {
        with_consign::<Self, _, _>(|consign| consign.len())
    }

    // consign_iter
}

pub struct Pure<T: 'static + Send + Sync + Hash + PartialEq> {
    ptr: NonNull<PureInner<T>>,
    phantom: PhantomData<PureInner<T>>,
}

unsafe impl<T: 'static + Send + Sync + Hash + PartialEq> Send for Pure<T> {}
unsafe impl<T: 'static + Send + Sync + Hash + PartialEq> Sync for Pure<T> {}

impl<T: 'static + Send + Sync + Hash + PartialEq> Pure<T> {
    // box like things

    #[inline]
    pub fn as_ref<'a>(&'a self) -> &'a T {
        &self.inner().data
    }

    #[inline]
    fn inner(&self) -> &PureInner<T> {
        unsafe { self.ptr.as_ref() }
    }
}

impl<T: 'static + Send + Sync + Hash + PartialEq> Pure<T> {
    pub fn new(data: T) -> Self {
        with_consign::<T, _, _>(|consign| {
            let (entry, _) = consign
                .raw_entry_mut()
                .from_key(&ConsignKey(&data))
                .or_insert_with(|| (ConsignEntry::new(data), ()));

            entry.inner().inc_ref()
        })
    }
}

impl<T: 'static + Send + Sync + Hash + PartialEq> Clone for Pure<T> {
    fn clone(&self) -> Self {
        self.inner().inc_ref()
    }
}

impl<T: 'static + Send + Sync + Hash + PartialEq> Drop for Pure<T> {
    fn drop(&mut self) {
        self.inner().dec_ref();
    }
}

impl<T: 'static + Send + Sync + Hash + PartialEq> Deref for Pure<T> {
    type Target = T;

    fn deref<'a>(&'a self) -> &'a T {
        self.as_ref()
    }
}

impl<T: 'static + Send + Sync + Hash + PartialEq> PartialEq for Pure<T> {
    fn eq(&self, other: &Self) -> bool {
        self.as_ref() == other.as_ref()
    }
}

impl<T: 'static + Send + Sync + Hash + PartialEq> Hash for Pure<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.as_ref().hash(state)
    }
}

impl<T: 'static + Send + Sync + Hash + PartialEq + Display> fmt::Display for Pure<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.as_ref().fmt(f)
    }
}

impl<T: 'static + Send + Sync + Hash + PartialEq + Debug> fmt::Debug for Pure<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.as_ref().fmt(f)
    }
}

struct PureInner<T>
where
    T: ?Sized,
{
    ref_count: AtomicUsize,
    data: T,
}

unsafe impl<T: ?Sized + Sync + Send> Send for PureInner<T> {}
unsafe impl<T: ?Sized + Sync + Send> Sync for PureInner<T> {}

impl<T> PureInner<T> {
    fn new(data: T) -> Box<Self> {
        Box::new(PureInner {
            ref_count: AtomicUsize::new(0),
            data,
        })
    }
}

impl<T: 'static + Send + Sync + Hash + PartialEq> PureInner<T> {
    fn inc_ref(&self) -> Pure<T> {
        if self.ref_count.fetch_add(1, Ordering::Relaxed) >= isize::MAX as usize {
            std::process::abort();
        }

        Pure {
            ptr: self.into(),
            phantom: PhantomData,
        }
    }

    fn dec_ref(&self) {
        if self.ref_count.fetch_sub(1, Ordering::Release) != 1 {
            return;
        }

        atomic::fence(Ordering::Acquire);

        with_consign::<T, _, _>(|consign| consign.remove_entry(&ConsignKey(&self.data)));
    }
}

/* CONSIGNATION
 *
 */

lazy_static! {
    static ref ALL_CONSIGN: Mutex<HashMap<TypeId, Box<dyn Any + Send + Sync>>> =
        Mutex::new(HashMap::new());
}

fn with_consign<T, F, R>(tx: F) -> R
where
    T: 'static + Send + Sync + Hash + PartialEq,
    F: FnOnce(&mut HashMap<ConsignEntry<T>, ()>) -> R,
{
    let mut type_map = ALL_CONSIGN.lock().unwrap();

    tx(type_map
        .entry(TypeId::of::<T>())
        .or_insert_with(|| Box::<HashMap<ConsignEntry<T>, ()>>::new(HashMap::new()))
        .downcast_mut::<HashMap<ConsignEntry<T>, ()>>()
        .unwrap())
}

#[derive(PartialEq)]
struct ConsignKey<'a, T: Hash + PartialEq>(&'a T);

impl<T: Hash + PartialEq> Hash for ConsignKey<'_, T>
where
    T: Hash,
{
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl<'a, T> Eq for ConsignKey<'a, T> where T: Hash + PartialEq {}

impl<T: Send + Sync + Hash + PartialEq> Equivalent<ConsignEntry<T>> for ConsignKey<'_, T>
where
    T: PartialEq,
{
    fn equivalent(&self, entry: &ConsignEntry<T>) -> bool {
        *self.0 == entry.inner().data
    }
}

#[derive(PartialEq)]
struct ConsignEntry<T: Send + Sync + Hash + PartialEq>(NonNull<PureInner<T>>);

unsafe impl<T: Send + Sync + Hash + PartialEq> Send for ConsignEntry<T> {}
unsafe impl<T: Send + Sync + Hash + PartialEq> Sync for ConsignEntry<T> {}

impl<T: Send + Sync + Hash + PartialEq> Eq for ConsignEntry<T> where T: Hash + PartialEq {}

impl<T: Send + Sync + Hash + PartialEq> ConsignEntry<T> {
    fn new(data: T) -> Self {
        let inner = PureInner::new(data);
        ConsignEntry(Box::leak(inner).into())
    }

    fn inner(&self) -> &PureInner<T> {
        unsafe { self.0.as_ref() }
    }
}

impl<T: Send + Sync + Hash + PartialEq> Drop for ConsignEntry<T> {
    fn drop(&mut self) {
        unsafe { drop(Box::from_raw(self.0.as_ptr())) }
    }
}

impl<T: Send + Sync + Hash + PartialEq> Hash for ConsignEntry<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.inner().data.hash(state);
    }
}

impl Consign for String {}
impl Consign for &'static str {}
impl<T: 'static + Send + Sync + Hash + PartialEq> Consign for Vec<T> {}
