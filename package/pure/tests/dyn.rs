extern crate static_assertions;

use std::{any::Any, hash::Hash, mem::size_of, sync::atomic::AtomicUsize, marker::PhantomData, ptr::NonNull};

use axis_pure::{Consign, Pure};
use static_assertions::assert_eq_size;

trait Trait {
    fn foo(&self) {}
}

#[derive(PartialEq, Eq, Hash, Consign, Debug)]
struct Type();

impl Trait for Type {}

struct Inner<T: ?Sized>(AtomicUsize, T);

struct Outer<T: ?Sized>(NonNull<Inner<T>>, PhantomData<Inner<T>>);

impl<T> Outer<T> {
    pub fn new(value: T) -> Self {
        let box_inner = Box::new(Inner(AtomicUsize::new(0), value));
        Self ( NonNull::new(Box::into_raw(box_inner)).unwrap(), PhantomData )
    }
}

impl<T: Trait+'static> Outer<T> {
    pub fn to_dyn(self) -> Outer<dyn Trait> {
        let Outer(ptr, _) = self;
        Outer(ptr, PhantomData)
    }
}


#[test]
fn simple_test3() {
    let outer: Outer<dyn Trait> = Outer::new(Type()).to_dyn();
}
