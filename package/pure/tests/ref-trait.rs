use std::ops::Deref;


/**
 * A reference to data of type T via underliying type U.
 */
trait Ref<T> {
    fn fetch(&self) -> &T;
}
