extern crate static_assertions;

use std::{any::Any, hash::Hash};

use axis_pure::{Consign, Pure};
use static_assertions::assert_eq_size;

#[derive(PartialEq, Eq, Hash, Consign, Debug)]
struct Generic<T: 'static + Sync + Send + Hash + Eq>(T);

#[derive(PartialEq, Hash, Consign, Debug)]
struct Point(usize, usize);

#[derive(PartialEq, Hash, Consign, Debug)]
struct Place(String, Pure<Point>);



assert_eq_size!(
    Pure<Place>,
    Pure<Option<Place>>,
    *const Place
);



#[test]
fn simple_test() {
    {
        assert_eq!(Generic::<i32>::consigned_count(), 0);

        let _a = Generic(32).pure();
        let _b = Generic(32).pure();
        let _c = Generic(33).pure();

        let s1 = "hola".pure();
        let s2 = "hola".pure();

        assert!(s1 == s2);
        assert!(_a == _b);
        assert!(_a != _c);

        println!("{}", _a.0 + 5);

        println!("{s1} {:?}", s1.as_ptr());
        println!("{s2} {:?}", s2.as_ptr());

        assert_eq!(Generic::<i32>::consigned_count(), 2);
    }

    assert_eq!(Generic::<i32>::consigned_count(), 0);
}



#[test]
fn simple_test2() {

    impl Place {
        pub fn hey(&self) {

        }
    }
    
    {
        let p0 = Point(5,1).pure();
        let p1 = Point(5,2).pure();

        let place = Place("hoaf".into(), p0).pure();
        
        place.hey();

        println!("{place:?}");
    }

}


#[test]
fn simple_test3() {

    let v = vec![1,2,3].pure();
    println!("{v:?}");

}
