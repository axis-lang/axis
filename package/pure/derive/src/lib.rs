extern crate proc_macro;
extern crate syn;
#[macro_use]
extern crate quote;

use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput};


#[proc_macro_derive(Consign)]
pub fn derive_consign(input: TokenStream) -> TokenStream {
    // Construct a string representation of the type definition
    let input = parse_macro_input!(input as DeriveInput);

    let ident = input.ident;

    let (impl_generics, ty_generics, where_clause) = 
        input.generics.split_for_impl();

    let expanded = quote! {
        #[automatically_derived]
        impl #impl_generics ::axis_pure::Consign for #ident #ty_generics #where_clause {
        }
    };

    TokenStream::from(expanded)
}

