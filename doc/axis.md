Axis unifica el concepto de trait, value (constant) y data model (layout):

Un trait no es mas que un dato constante, que cumple una serie de condiciones,


cuando un trait implementa o extiende otro trait, 

por ejemplo un Layout es un enum de los tipos de layout soportados.


```


def Layout:
	expose bitlenght: Natural
	expose alignment: Natural

def Layout::Basic<bitlenght: Natural, alignment: Natural>: 
	extends Layout {
		bitlength, 
		alignment
	}

def Layout::Tuple<items: Sequence<Layout>>: // Secuancia acotada (tiene longitud)
	extends Layout {
		bitlength: calculated_bitlenght
		alignment: calculated_alignment
	}


def Scalar extends Numeric
def Real extends Scalar
def Natural extends Scalar
def Real extends Scalar
def Complex extends Scalar
def Array extends Numeric

def Nat32 extends:
	Natural
	Layout::Basic(bitlenght = 32, alignment = 64)

// a es una funcion (por que define un retorno, lo cual implica transformacion)
def add(a: Numeric, b: Numeric) => Numeric

impl add(a: Nat32, b: Nat32) => Nat32:
	... low level specific code





```
