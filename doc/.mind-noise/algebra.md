# Type algebra

```

overload foo(..) -> 


overload foo(a: int) -> a + 1
overload foo(a: int, b: int) -> a + b


dispath x {
    (a: int) -> a + 1
    (a: int, b: int) -> a + b
}

```
