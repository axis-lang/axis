# Traits and layouts

## Disjuntive unions (|)

```
trait Person {
    property name: String
    proeprty surname: String | None
}

trait Location {
    .coordinates: (lat: Real, Long: Real)

}

p1 = Person(name: 'Peter', surname: 'pan')
p2 = Person(name: 'John')

if Person(name, surname: None) = p1 {
    // never because surname is String
}

match p2.surname {
    (surname: String) -> {

    }
    (surname: None) -> {

    }

}

```




la union de dos layouts A | B genera disyuccion 
la union de dos traits A + B genera conjuncion

## Union and discriminants

Algunos discriminantes de una union pueden estar presentes en el layout contenedor

```
enum foo::Coroutine = {
    Entry: (:int, :int) -> foo::Coroutine = foo::entry
}

layout foo::Continuator: Tuple = (
    EntryPoint
    Point1
    Point2
    Finished
)
```

## Layout analogies

**Tuple <-> Array**
```
let coords1: Real[2] = (latitude: 45.2, longitude: 93.1) // tuple to array coercion
let coords2: (latitude: Real, longitude: Real) = [4.23, 0.13]; // array to tuple 
```

**squeeze/unsqueeze**
Tuples y arrays con longitud 1 es analogo al unico elemento que contiene, y viceversa
```
let name: String = (name: "John Doe")
```

**Singleton unions are alias**
a: {Entry :Natural, }

## Disjuctions (enums and unions)

Una union 






## Traits

