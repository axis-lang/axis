# Languaje design



El diseño de lenguaje axis esta fundamentado en dar al programador 
flexibilidad a la hora de escribir codigo mas implicito y/o generico 
a mas explicito y/o optimizado.

La parte implicita es lo que ocurre en el backstage.




inmutable is the default behaviour, both fn declarations are equivalent
```axis
fn sin(inm alpha: number) -> number

fn sin(alpha: number) -> number

```

```axis

impl fn sin(value: Number) where
    value->layout

```




## pattern matching





fn sin()




sin(
    inm a: number::{layout: Float32}, 
    inm b: number::{layout: Float32},
) -> number::{layout: Float32}



foo(inm value: number) -> :
where value->


## currying and lazy and pipes

```
let half = (_, 2) <- div;

half(10) == 5

let curry = (10, 20) <- add;

curry() == 30

range(10)
    <- map(x -> x * 10)
    <- filter(x -> x % 10 )
```

es igual a filter(...)(map(...)(range(...)))