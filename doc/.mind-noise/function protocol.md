# Function protocol


## context injection

La inyeccion de contexto ocurre cuando al realizar el lowering, una funcion puede 
requerir de mas parametros de los que explicitamente recibe en su llamada. 

Por ejemplo en los casos de ContinuationContext o AsyncContext.

veamos un ejemplo de injeccion de ambos contextos:

```axis

trait TextStream {
    read_line() -> Text
}

fn read_lines(path: Path) -> Seq<Text> {
    
    with path: TextStream(stream) -> {
        
        yield stream.read_line()
    }

}
```

En este ejemplo la construccion sintactica 'with path: TextStream' aplica un Closure

with hash.entry(key): Enytr 
