# References

## Languaje design
**Type inference**
- [Hindley–Milner type system](https://en.wikipedia.org/wiki/Hindley%E2%80%93Milner_type_system)
- [rustc-infer](https://github.com/rust-lang/rust/tree/master/compiler/rustc_infer/src/infer)

- [What part of Hindley-Milner do you not understand?](https://stackoverflow.com/questions/12532552/what-part-of-hindley-milner-do-you-not-understand)