Interfaces


Al igual que los Layouts, interface describe a alto nivel las propiedades de las entidades.


```

def Array {
}

extends Array with Numeric 

extends Array as Interface {
	prop rank: Natural;
	prop shape: Shape;
}

extends Array as Indexable {

}

```
