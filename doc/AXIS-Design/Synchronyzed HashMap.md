Un hashmap compartido entre los diferentes cores que permite multiples lectores y un escritor simultaneos: el mutex solo se adquiere para escribir, la lectura no es bloqueante.

El principio de funcionamiento es simple:
 - el hashmap estará disponible para lectura en todo momento
 - cualquier operación de lectura debe efectuarse mediante una operación atomica para testear el bit de commit.
 - se reserva un bit para indicar si una entrada en el hashmap ha sido efectuada (commited)
 - Una operacion de escritura intentará realizarse sin realojar la tabla, la operacion escribe en la entrada y el bit de commit se mantiene en todo momento desactivado hasta el final
 - si la operacion de escritura requiere de un realojar la tabla, entonces se utiliza la tabla previa para continuar pudiendo efectuar lectura mientras el hilo de escritura.