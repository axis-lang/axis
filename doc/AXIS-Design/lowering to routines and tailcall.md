AXIS generaliza toda funcion al concepto de funcion asincrona/generador/corutina. 

por lo que el valor retornado por una funcion puede ser cualificado como 'continuador' 

cuando una funcion A llama a una funcion X y esta arroja (yield) un valor, ambas funciones son tratadas como corutinas, 

## generalizacion como corutina de una rutina convencional

El retorno de la funcion real `sin(x)` puede generalizarse como `coro[Finish] real`, `coro[Finish]` es el cualificador que recibe una corutina que ha finalizado.

```rust
def foo() {
	if random() > .5 {
		yield int 5
	}
	else {
		yield float 3.14
	}
	return "goodbye"
}
```
en este caso, la función `foo()` devuelve `coro[..] {int,float}` 

Ejemplo de herencia de cualificadores:
 - El cualificador`async` deriva de `coro`. 
 - El cualificador `gen` deriva de `coro`.
 - El cualificador `asyncgen` deriva de `async` y de `gen` que derivan de `coro`.

un coro puede ser capturado (desestructurado) por el caller, retomando el control, o puede continuar recorriendo la pila de llamadas. Visto desde abajo a arriba, un event loop que solo captura los valores arrojados tipo E solo puede invocar corutinas que arrojan ese tipo de valor. Las corutinas invocadas por E deben capturar todos los valores diferentes del tipo E y manejarlos, pudiendo arrojar valores tipo E manejados por el caller (el evento loop).

Una funcion valida para un event loop que maneja el tipo E sería:
`entry_point() -> Coro[E] Void`


`Coro[E] Void` 

## Trayendo al caso los valores INOUT 

Podemos comprender como participan los valores de entrada y salida (argumento y retorno) con las corutinas con el siguiente codigo:
```rust 
fn coro(self, a: A) -> (self, R) { return (self+1, )}


// llama a la corutina, 
// dropeando el coro como argumento
// pero continuando con bar
let (bar, b) = foo(a); 

// el tipo de bar podria haber cambiado, 
// y puede ser necesario machearlo. antes de continuar
// hay tipos que podrian no matchearse, por ejemplo los asincronos
// necesitando arrojarlo hacia atrás, pero pudiendo no hacerlo en este momento
```

desde el punto de vista del event loop (una funcion que no puede ser una corutina), éste mantiene el estado de una corutina en una variable propia: 
```js
var my_coro = ...;
while(true) {
	// llama a micoro indefinidamente
	match my_coro() {
		coro[E] Void
	}
}
```

Una corutina es un objeto que se modifica a si mismo en cada llamada, cambiando el valor de la indireccion de la llamada.
## cualificador own (opuesto de shared)
