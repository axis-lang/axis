

**Estructurar**
```ts
let order = Order {
	user: Profile {
		name: Text 'John doe'
		birthdate: Date '2001-23-45'
	},
	metadata: {
		sons: Natural 3
	},
	tags: Set<Text>[ 'alpha', 'beta' ]
}
```

**Desestructurar**
```ts
Order{ user, metadata: metadata, ... order_rest} = order;

order_rest.tags;
// alpha, beta

```


**Domain**
Un dominio es una estructura de datos que define y delimita el contenido otra estructura de datos.
```ts
const domain = Order {
	user: Profile,
	metadata: Union{  },
	tags: Set<Tags>
}

def Order:
	def profile: Profile {
		name: Text
		birthdate: Date
	}
	metadata: Object
	
	def tags: Set<Tag: Constrained<Text> {
		max_length: 40,
		format: Constrained<Text>::SLUG
	}>

Domain::of(Order)

```


**Layout**
El layout se construye para un dominio. 

```ts
Layout::of_domain(Domain::of(Order))
Layout::of(Order)
```
