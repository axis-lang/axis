## Generalizacion de cualificadores

En lenguajes como C tenemos el concepto de cualificadores de tipo (const, volatile, auto, etc), compañado de otros conceptos agregados al sistema de tipado pero no generalizados (no son ciudadanos de primera clase como las estructuras), como los arrays y los punteros

AXIS generaliza junto con la idea de tipos, la idea de cualificadores (que dan cualidad). Los cualificadores pueden ser utilizados de forma muy variada, por ejemplo dar forma (Array extents), definit acceso (tipos remotos), diferir valores (promesas y futuros), observabilidad, volatilidad.

Los cualificadores ponen fuera del tipo la logica con la que python maneja las metaclases, en el caso de python, las metaclases sirven como una infraestructura del tipo, definen el tipo del tipo permitiendo polimorfismo de tipos (Herencia). Esa Practica puede operar en el diseño de axis. pero los cualificadores vienen a poner fuera del tipo gran parte de la logica que en lenguajes dinamicos como python y java suceden bajo el capo (acceso a tributos, llamadas a metodos, propiedades de las clases, metodos magicos etc) y en otros lenguajes como Rust o C++ suceden de forma inherente a al lenguaje.

### ¿Qué es un cualificador?
Un cualificador es un objeto que envuelve a un tipo actuando como middleware entre lo que el tipo tipa (el valor) y lo que no es tipado por el tipo (el contexto).

**Sintaxis**
La cualificacion forma parte de la gramatica de tipos (definir regla gramatical) tiene precedencia de derecha a izquierda 

La expresion`extent[4,4] real[32]` define una matriz de 4x4 de valores reales de 32 bits.
 - `extent[4, 4]` actua de cualificador de `real[32]`
 - `extent` es el simbolo que identifica la entidad que actua de cualificador de tipo (as_type_qualifier)
 - `[4, 4]` es el argumento de especificacion del cualificador
 - `real` por ser el ultimo simbolo en la composicion es el simbolo que identifica la entidad tipante
 - `[32]` es el argumento de especificacion del tipo

### Como se construye un cualificador de 

```
# fixed length vector implementation
_def_ vec[Length: Nat] T: type {
	
}

# dynamic vector implementation
_def_ vec T: type {

} 

```

### Para que los cualificadores
los 

```python


def my_cualifier()
```





```python
def is_assignable(src:Type, dst:Type) -> bool:
	return is_subtype(src, dst)
```

## Semantic typing:

### Ordered vs Indexed
Los tipos pueden presentar caracteristicas de orden e indexacion. El orden y la indexacion es un aspecto fundamental a la hora de trabajar con la semantica de tipos. 

Por ejemplo, el tuple `(name: 'John', surname: 'Doe')`  se compone como un tipo ordenado `('John', 'Doe')` con longitud 2 y claves 0 y 1, así como un tipo indexado `{'name': 'John', 'surname': 'Doe'}. 

 - Tuple (structure): `(name: 'John', surname: 'Doe')`
	 - un tuple se comporta como un listado y un diccionario, 
	 - 
