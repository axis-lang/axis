Para ejemplificar la potencia de los cualificadores, un tipo de dato `cuda tensor number` podria generalizar a cualquier tipo de tensor manejado por librerias como torch o tensorflow.

`cuda tensor` implica un objeto que reside en una memoria remota, tensor no especifica el numero de dimensiones aunque podria hacerlo, y cuda podria diseñarse de forma mas generica denominandolo gpu.

cuda viene a decir que se debe conocer un identificador del dispositivo. tensor establece informacion ndimensional

la implementacion y especializacion del layout podria capturar la parte de cuda tensor 

```rust
def Layout { 
	// Definicion general de un layout
	// un layout es una estructura de datos mas o menos plana que representa la disposicion de informacion en la memoria fisica.
}

def Layout for cuda tensor .. { // especificacion para un cuda tensor
	// el layout de un cuda tensor ...
}

impl Layout {

}
```

``` rust
var tensor: own cuda tensor real[32] = zeros(32, 32);

let tensor = add(tensor, tensor);

::sys::mem::layout_of(cuda tensor real) -> Layout
```


```rust

fn add[T: Number](a: .. T, b: .. T) -> .. T
brief
	Este bloque es capturado completamente como un bloque de documentacion
	debe estar identado y todo el texto es 

where 
	
;

impl add[T: Number](a: Remote Tensor T, b: Remote Tensor T) -> Remote Tensor T;



```