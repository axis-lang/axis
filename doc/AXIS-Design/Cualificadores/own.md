el cualificador own implica que un recurso es propio, desde un punto de vista de la composicion de datos una estructura anidada de datos el tipo:
```json
{
	"embedded": {
	},
}
```
Define un objeto que contiene otro objeto, el objeto interior es poseido por el objeto exterior. y este tiene una referencia unica. En C seria representado como una estructura anidada, pero en lenguajes como python o js son dos objetos independientes que residen en el heap. desde el punto de vista de rust own seria un box/vec/String, un tipo de dato con un solo propietario.

el concepto de propiedad unica entra a jugar con los lifetimes de rust. 