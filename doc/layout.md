
`T: Layout`: T implementa un layout (T tiene tamaño)




```
def Layout<DataSpec: ...> {
	const size: Natural = abi.sizeof(DataSpec);
	const align: Natural = abi.alignof(DataSpec);
}


```

* `DataSpec :> Sequence<Layout>`: Tuple, ejemplo `Layout[Natural, Natural, Integer]`
* `DataSpec :> Shaped<Layout>`: Array,  `Layout[Natural[2,3,4]]`
* `DataSpec :> Set<Layout>`: Union, `{Natural, Float}`
* `DataSpec :> Sequence<Named<Layout>>`: Structure, `Layout<(alpha: Natural, beta: Natural)>`
* `DataSpec :> Set<Named<Layout>>`: Enum(rust-like), `Event: Layout<{OnClick: ..., OnMove:...}}>`
* `DataSpec :> Number`: Atom, `n32: Layout<32>`


## Auxiliar traits

```

def Name: Contract<Text, Rex'[a-z]...*'>
def Named<T>: Map<Name, T>

def Shape<...T>: Natural[...T] {

}

def Sequence<T> {
}

def Set<T> {
}

def Layout<DataSpec, Align=None> {
...
} 

def Number {}
def Scalar: Number {}
def Natural: Scalar {}
def Integer: Scalar {}
def Complex: Scalar {}
def Real: Scalar {}
def N<Depth: Natural>: Natural, Layout<Depth>
def R<Depth: Natural>: Real, Layout<Depth>
	where Depth in REAL_VALID_DEPTHS else Error('Invalid real depth {Depth}')
def R32: Natural, Layout<32>


```
