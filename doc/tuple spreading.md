# the spread operator in tuples and arguments

```

```

```rust
fn foo(x, y, ..c) {
	bar(..c)
}

fn bar(z: nat) {
	let a = if z > 0 {
		a
	} 
	else {
		_
	}

	/
}

foo(1, y: 2, z: 3)

```
