use hashbrown::HashMap;

fn main() {
    //println!("Hello, world!");

    let mut hm = HashMap::new();
    hm.insert(6, 7);
    hm.raw_entry_mut().from_key(&8).or_insert_with(|| {(8, 9)});

    for (k, v) in hm.iter() {
        println!("Entry {} {}!", k, v);
    }

}
